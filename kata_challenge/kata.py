import json


class CheckOut(object):

    def __init__(self):
        with open("rules.json") as prices:
            self.prices = json.load(prices)

    cart = {"A": 0,
            "B": 0,
            "C": 0,
            "D": 0}

    def scan(self, item):

        self.cart[item] += 1

    def calculate_total(self):

        total = 0

        for item in self.cart:
            # Se o item nao tem preco especial, entao apenas adicione os precos normais ao total
            # print(self.prices[item]["special_price"][0])

            item_price = self.prices[item]["price"]
            amount_item_cart = self.cart[item]

            # if the actual item doesnt have a special price
            if self.prices[item]["special_price"] == []:

                # just multiply the price by the quantity in the cart
                total += item_price * amount_item_cart

            else:

                special_price = self.prices[item]["special_price"][1]
                special_price_quantity = self.prices[item]["special_price"][0]

                mod = amount_item_cart % special_price_quantity

                # ok, this makes our life simpler
                if mod == 0:
                    total += (amount_item_cart / special_price_quantity) * special_price
                else:
                    total += (item_price * mod) + (special_price * (amount_item_cart / special_price_quantity))

        return total


co = CheckOut()
co.scan("A")
co.scan("B")
co.scan("B")
co.scan("C")
co.scan("C")
co.scan("C")
co.scan("C")
co.scan("C")
co.scan("D")
co.scan("D")
co.scan("D")
print(co.calculate_total())
