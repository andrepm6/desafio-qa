## Sobre o desafio

Este desafio foi feito utilizando o behave e appium, e a linguagem utilizada foi o python versão 2.7. Certifique-se que a versão correta esteja instalada.

Os testes foram desenvolvidos para serem executados em um dispositivo Android 7.1.1. Para executar em outros disposivitos e em outras versões, altere as desired capabilities no arquivo environment.py.

O padrão utilizado no projeto, é uma variação do Robot Pattern introduzido por Jake Wharton (http://jakewharton.com/testing-robots/) 

### Pré-requisitos

Python 2.7:

https://www.python.org/downloads/.

Behave, appium e subprocess

```
pip install appium-python-client
pip install behave
pip install subprocess
```

## Executando

Antes de executar os testes, é necessário iniciar o servidor do appium.

Para executar os testes, execute no diretório onde estão os arquivos feature:

```
behave
```

