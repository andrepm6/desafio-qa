# Created by andre.mendes at 21/11/2017
Feature: Calls
  In order to keep in touch with my contacts
  As a user
  I want to be able to make calls

  Background:
    Given the contact "Gabrielle Honorato" is selected

  Scenario: Start a voice call
    When the button "voice call" is pressed
    Then the screen "voice call" is displayed

  Scenario: Start a video call
    When the button "video call" is pressed
    Then the screen "video call" is displayed
    
