# coding: utf-8
import os
import subprocess
from appium import webdriver


def before_scenario(context, *args):

    app = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../artifacts/whatsapp.apk'))

    desired_caps = {'appActivity': 'com.whatsapp.HomeActivity',
                    'platformName': 'Android',
                    'platformVersion': '7.1.1',
                    'deviceName': 'Moto G (5)',
                    'app': app,
                    'noReset': 'true'}

    context.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)


def after_scenario(context, *args):

    context.driver.quit()


def after_all():

    # enables GPS again
    subprocess.check_output('adb shell settings put secure location_providers_allowed +gps')
