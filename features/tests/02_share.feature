# Created by andre.mendes at 21/11/2017
Feature: Share

  Background:
    Given the contact "Gabrielle Honorato" is selected
    And the button "message" is pressed
    When the button "add attachment" is pressed

  Scenario: Contact share
    And the option "contact" is selected
    Then the contact "Gabrielle Honorato" is selected
    And the button "next" is pressed
    When the button "send" is pressed
    Then the card "contact" is displayed in the conversation

  Scenario: Location share
    And the option "location" is selected
    When the button "send current location" is pressed
    Then the card "current location" is displayed in the conversation

  Scenario: Location with GPS disabled
    And disable the gps
    When the option "location" is selected
    Then the screen "GPS disabled" is displayed
