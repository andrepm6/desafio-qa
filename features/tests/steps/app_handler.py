# -*- coding: utf-8 -*-
from behave import *
from util import android_routines


@step('the contact "{contact}" is selected')
def select_contact(context, contact):

    android_routines.select_contact(context.driver, contact)


@step('the screen "{screen}" is displayed')
def check_screen(context, screen):

    android_routines.find_screen(context.driver, screen)


@step('the button "{btn}" is pressed')
def press_button(context, btn):

    android_routines.press_button(context.driver, btn)


@step('the option "{option}" is selected')
def choose_option(context, option):

    android_routines.select_option(context.driver, option)


@step('the card "{card}" is displayed in the conversation')
def find_card(context, card):

    android_routines.find_card(context.driver, card)


@step('disable the gps')
def gps_handler(context):

    android_routines.disable_gps()

