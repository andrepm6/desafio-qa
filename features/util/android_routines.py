# -*- coding: utf-8 -*-
from util import elements_constants


def select_contact(driver, contact):

    # Makes the search with uiautomator. Grabs the first scrollable element, and scrolls to the desired element
    desired_contact = driver.find_element_by_android_uiautomator(
        'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().description("{0}"));'.format(contact))

    desired_contact.click()


def find_screen(driver, screen):

    elem = elements_constants.SCREENS[screen]

    if screen == 'video call':
        driver.find_element_by_android_uiautomator('new UiSelector().description("{0}")'.format(elem))
    else:
        driver.find_element_by_id(elem)


def press_button(driver, btn_name):

    if btn_name == "back":
        driver.back()
        return

    else:
        elem = elements_constants.BUTTONS[btn_name]
        btn = driver.find_element_by_id(elem)

    btn.click()


def select_option(driver, option):

    elem = elements_constants.OPTIONS[option]

    if option in ['contact', 'location']:

        elem = driver.find_element_by_id(elem)

    else:

        # Makes the search with uiautomator. Grabs the first scrollable element, and scrolls to the desired element
        elem = driver.find_element_by_android_uiautomator(
            'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().description("{0}").instance(0));'.format(elem))

    elem.click()


def find_card(driver, card):

    elem = elements_constants.CARDS[card]

    driver.find_element_by_id(elem)


def disable_gps():

    import subprocess

    # spawn a process to disable gps using adb
    subprocess.check_output('adb shell settings put secure location_providers_allowed -gps')
