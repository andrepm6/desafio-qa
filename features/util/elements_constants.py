# -*- coding: utf-8 -*-

OPTIONS = {'contact': 'pickfiletype_contact',
           'location': 'pickfiletype_location'}

SCREENS = {'contacts': 'contactpicker_row_photo',
           'GPS disabled': 'alertTitle',
           'voice call': 'call_status',
           'video call': 'Alternar Câmera'}

BUTTONS = {'add attachment': 'input_attach_button',
           'voice call': 'audio_call_btn',
           'message': 'message_btn',
           'next': 'next_btn',
           'send': 'send_btn',
           'send current location': 'send_current_location_text',
           'video call': 'video_call_btn'}

CARDS = {'contact': 'contact_card',
         'current location': 'thumb_button'}
